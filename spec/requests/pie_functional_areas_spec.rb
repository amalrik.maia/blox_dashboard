require 'rails_helper'

RSpec.describe 'Charts::PieFunctionalAreas', type: :request do
  let!(:blox_with_associations) { create(:blox_with_associations) }

  include_examples 'json responses', '/charts/pie_functional_areas/show'
end
