require 'rails_helper'

RSpec.describe 'Charts::ColumnBloxes', type: :request do
  let!(:blox_with_associations) { create(:blox_with_associations) }

  include_examples 'json responses', '/charts/column_bloxes/show'
end
