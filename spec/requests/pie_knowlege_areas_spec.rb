require 'rails_helper'

RSpec.describe 'Charts::PieKnowlegeAreas', type: :request do
  let!(:blox_with_associations) { create(:blox_with_associations) }

  include_examples 'json responses', '/charts/pie_knowlege_areas/show'
end
