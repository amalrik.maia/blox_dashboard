require 'rails_helper'

RSpec.describe 'Dashboard', type: :request do
  it "Renders the Dashboard" do
    get '/dashboard/index'
    expect(response).to render_template(:index)
    expect(response.body).to include('Visão geral do progresso dos Blox na instituição')
  end
end
