RSpec.shared_examples 'json responses' do |url|
  it "returns a json response" do
    get url

    expect(response.content_type).to eq("application/json; charset=utf-8")
    expect(valid_json? response.body).to be_truthy
  end
end
