require 'rails_helper'

RSpec.describe Blox do
  let!(:bloxes) { create_list(:blox, 10) }
  let!(:blox_with_associations) { create(:blox_with_associations) }

  let(:blox_categories) {
    [
      OpenStruct.new(name: 'Ciclo', category: 'cycle'),
      OpenStruct.new(name: 'Turno', category: 'shift')
    ]
  }

  context 'relationships' do
    it { should have_many(:knowlege_areas) }
    it { should have_many(:functional_areas) }
    it { should have_many(:blox_profiles) }
    it { should have_many(:cycles) }
    it { should have_many(:shifts) }
  end

  describe '.categories' do
    subject { Blox.categories }
    it { is_expected.to eq(blox_categories) }
  end

  describe '.count_by_table_and_blox_title' do
    context 'with cicles as parameter' do
      it 'returns a hash with bloxes count grouped by cicle' do
        expect(Blox.count_by_table_and_blox_title(:cycles, 'cycles.name')).to eq({['Ciclo', blox_with_associations.title]=>3})
      end
    end
    context 'with shift as parameter' do
      it 'returns a hash with bloxes count grouped by shift' do
        expect(Blox.count_by_table_and_blox_title(:shifts, 'shifts.name')).to be_a(Hash)
        expect(Blox.count_by_table_and_blox_title(:shifts, 'shifts.name')).to include([Shift.pluck(:name).sample, blox_with_associations.title] )
      end
    end
  end
end
