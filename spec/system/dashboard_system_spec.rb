require "rails_helper"

RSpec.describe "Dashboard", :type => :system do
  let!(:blox_with_associations) { create(:blox_with_associations) }

  before do
    driven_by(:selenium_chrome_headless)
  end

  it "enables me to create widgets" do
    visit "/dashboard/index"

    expect(page).to have_text("Visão geral do progresso dos Blox na instituição")
    expect(page).to have_css('div#chart-1', :count => 1)
    expect(page).to have_css('div#chart-2', :count => 1)
    expect(page).to have_css('div#chart-3', :count => 1)
    expect(page).to have_css('div#chart-4', :count => 1)
  end
end