require 'faker'

FactoryBot.define do
  factory :blox do
    title { Faker::Educator.course_name }

    factory :blox_with_associations do
      transient do
        elements_count { 3 }
      end

      after(:create) do |blox, evaluator|
        create_list(:shift, evaluator.elements_count, blox: blox)
        create_list(:cycle, evaluator.elements_count, blox: blox)
        create(:blox_profile, blox: blox)
        create(:functional_area, blox: blox)
        create(:knowlege_area, blox: blox)
      end
    end
  end
end
