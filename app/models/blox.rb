class Blox < ApplicationRecord
  has_many :knowlege_areas
  has_many :functional_areas
  has_many :blox_profiles
  has_many :cycles
  has_many :shifts

  def self.categories
    [
      OpenStruct.new(name: 'Ciclo', category: 'cycle'),
      OpenStruct.new(name: 'Turno', category: 'shift')
    ]
  end

  def self.count_by_table_and_blox_title(join_table, group_by_attribute)
    joins(join_table).group(group_by_attribute).group('bloxes.title').count
  end
end
