module DashboardHelper
  def donnut_options(blox_total)
    {
      donut: true,
      messages: { empty: 'No data' },
      library: { cutoutPercentage: 70, elements: { center: { text: "#{blox_total}"}}} }
  end

  def column_options
    { stacked: true, messages: { empty: 'No data' } }
  end
end
