class DashboardController < ApplicationController
  def index
    @blox_count = Blox.count
    @blox_categories = Blox.categories
    @selected_blox_category = selected_blox_category
  end

  private

  def selected_blox_category
    dashboard_params.fetch(:blox_by, 'cycle')
  end

  def dashboard_params
    params.permit(:blox_by)
  end
end
