class Charts::PieFunctionalAreasController < ApplicationController
  def show
    render json: Blox.joins(:functional_areas).group("functional_areas.name").count
  end
end
