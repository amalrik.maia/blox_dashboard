class Charts::PieKnowlegeAreasController < ApplicationController
  def show
    render json: Blox.joins(:knowlege_areas).group("knowlege_areas.name").count
  end
end
