class Charts::ColumnBloxesController < ApplicationController
  def show
    render json: Blox.count_by_table_and_blox_title(join_table.to_sym, group_by_attribute).chart_json
  end

  private

  def group_by_attribute
    "#{join_table}.name"
  end

  def join_table
    column_blox_params.fetch(:column_blox, 'cycle').pluralize
  end

  def column_blox_params
    params.permit(:column_blox)
  end
end
