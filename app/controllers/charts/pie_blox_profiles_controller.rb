class Charts::PieBloxProfilesController < ApplicationController
  def show
    render json: Blox.joins(:blox_profiles).group("blox_profiles.name").count
  end
end
