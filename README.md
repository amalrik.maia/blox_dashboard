# README

This README will guide you through the steps necessary to get the application up and running.

* Ruby version
2.6.3

* System dependencies
  - Nodejs
  - Postgres

* Setup
```
$ bundle install
$ bin/rails db:create db:migrate db:seed
$ bin/rails s
```

you can now interact with the application on `localhost:3000`

* How to run the tests
`$ bundle exec rspec `

