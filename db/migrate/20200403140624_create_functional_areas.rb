class CreateFunctionalAreas < ActiveRecord::Migration[6.0]
  def change
    create_table :functional_areas do |t|
      t.string :name
      t.references :blox, null: false, foreign_key: true

      t.timestamps
    end
  end
end
