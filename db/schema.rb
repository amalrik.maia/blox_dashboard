# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_04_03_140805) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "blox_profiles", force: :cascade do |t|
    t.string "name"
    t.bigint "blox_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["blox_id"], name: "index_blox_profiles_on_blox_id"
  end

  create_table "bloxes", force: :cascade do |t|
    t.string "title", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "cycles", force: :cascade do |t|
    t.string "name"
    t.bigint "blox_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["blox_id"], name: "index_cycles_on_blox_id"
  end

  create_table "functional_areas", force: :cascade do |t|
    t.string "name"
    t.bigint "blox_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["blox_id"], name: "index_functional_areas_on_blox_id"
  end

  create_table "knowlege_areas", force: :cascade do |t|
    t.string "name"
    t.bigint "blox_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["blox_id"], name: "index_knowlege_areas_on_blox_id"
  end

  create_table "shifts", force: :cascade do |t|
    t.string "name"
    t.bigint "blox_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["blox_id"], name: "index_shifts_on_blox_id"
  end

  add_foreign_key "blox_profiles", "bloxes"
  add_foreign_key "cycles", "bloxes"
  add_foreign_key "functional_areas", "bloxes"
  add_foreign_key "knowlege_areas", "bloxes"
  add_foreign_key "shifts", "bloxes"
end
