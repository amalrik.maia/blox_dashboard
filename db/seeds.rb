# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

bloxes = Blox.create([{title: 'matematica'}, {title: 'matematica II'}, {title: 'matematica III'}, {title: 'matematica'}])
KnowlegeArea.create(name: 'Quantitativo', blox: bloxes.first)
KnowlegeArea.create(name: 'Técnico', blox: bloxes.second)
FunctionalArea.create(name: 'Quantitativo', blox: bloxes.first)
FunctionalArea.create(name: 'Quantitativo II', blox: bloxes.first)
FunctionalArea.create(name: 'Técnico', blox: bloxes.second)
BloxProfile.create(name: 'Quantitativo', blox: bloxes.first)
BloxProfile.create(name: 'Técnico', blox: bloxes.second)
Cycle.create(name: 'Ciclo', blox: bloxes.first)
Cycle.create(name: 'Ciclo', blox: bloxes.second)
Cycle.create(name: 'Ciclo II', blox: bloxes.fourth)
Shift.create(name: 'Matutino', blox: bloxes.first)
Shift.create(name: 'Matutino', blox: bloxes.second)
Shift.create(name: 'Vespertino', blox: bloxes.third)
Shift.create(name: 'Vespertino', blox: bloxes.fourth)
