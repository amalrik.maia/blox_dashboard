Rails.application.routes.draw do
  get 'dashboard/index', as: :dashboard
  get 'charts/pie_knowlege_areas/show', as: 'pie_knowlege_areas_charts'
  get 'charts/pie_functional_areas/show', as: 'pie_functional_areas_charts'
  get 'charts/pie_blox_profiles/show', as: 'pie_blox_profiles_charts'
  get 'charts/column_bloxes/show', as: 'column_bloxes_charts'
  root 'dashboard#index'
end
